﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraBehaviour : MonoBehaviour
{
    public GameObject block;
    private GameObject block_model;

    // Start is called before the first frame update
    void Start()
    {
        block_model = block.transform.Find("block model").gameObject;
    }

    // Update is called once per frame
    void Update()
    {
        Vector3 block_pos = block_model.GetComponent<MeshRenderer>().bounds.center;
        transform.position = block_pos + new Vector3(-0.77f - 0.5f*0.2f, 1.92f - block_pos.y, -1.12f - 0.5f*0.2f);
    }
}
