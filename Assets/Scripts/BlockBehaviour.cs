﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockBehaviour : MonoBehaviour
{
    private bool rotating = false;
    private Vector3 rotation_axis_point;
    private Vector3 rotation_axis_dir;
    private float rotation_current_value;
    private float rotation_final_value;
    private float speed;
    private Action on_finish;

    // Start is called before the first frame update
    void Start()
    {
    }

    public void startRotationAnimation(Vector3 axis_point, Vector3 axis_dir, float degree, Action on_finish) {
        this.rotation_axis_point = transform.parent.TransformPoint(axis_point);
        this.rotation_axis_dir = transform.parent.TransformDirection(axis_dir);
        this.rotation_current_value = 0f;
        this.rotation_final_value = degree;
        this.on_finish = on_finish;
        this.speed = Math.Abs(degree) / 0.2f;
        this.rotating = true;
    }

    // Update is called once per frame
    void Update()
    {
        if (this.rotating) {
            float next_value;
            if (rotation_current_value < rotation_final_value) {
                next_value = rotation_current_value + Time.deltaTime * speed;
                if (next_value > rotation_final_value) {
                    next_value = rotation_final_value;
                }
            } else if (rotation_current_value > rotation_final_value) {
                next_value = rotation_current_value - Time.deltaTime * speed;
                if (next_value < rotation_final_value) {
                    next_value = rotation_final_value;
                }
            } else {
                throw new System.Exception();
            }
            Transform t = transform;
            t.RotateAround(rotation_axis_point, rotation_axis_dir, next_value - rotation_current_value);
            if (next_value == rotation_final_value) {
                this.rotating = false;
                this.on_finish();
            } else {
                rotation_current_value = next_value;
            }
        }
    }

    public bool isAnimating() {
        return rotating;
    }
}
