﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour
{
    public GameObject block;
    private BlockBehaviour block_behaviour;
    public GameObject tile_pf;
    private char[,] current_map;
    private class BlockPosition {
        public int xmin, xmax;
        public int ymin, ymax;

        public BlockPosition(int initx, int inity) {
            xmin = initx;
            ymin = inity;
            xmax = xmin + 1;
            ymax = ymin + 1;
        }
        public BlockPosition(BlockPosition copy) {
            xmin = copy.xmin;
            xmax = copy.xmax;
            ymin = copy.ymin;
            ymax = copy.ymax;
        }

        public bool standing() {
            return xmin + 1 == xmax && ymin + 1 == ymax;
        }

        public bool lying_x() {
            return xmin + 2 == xmax;
        }

        public bool lying_y() {
            return ymin + 2 == ymax;
        }

        public void apply(Transform t) {
            if (this.standing()) {
                t.localPosition = new Vector3((float) xmin, 0, (float) ymin);
                t.localRotation = Quaternion.identity;
            } else if (this.lying_x()) {
                t.localPosition = new Vector3((float) xmin, 1f, (float) ymin);
                t.localRotation = Quaternion.AngleAxis(-90, new Vector3(0f,0f,1f));
            } else {
                t.localPosition = new Vector3((float) xmin, 1f, (float) ymin);
                t.localRotation = Quaternion.AngleAxis(90, new Vector3(1f,0f,0f));
            }
        }
    }
    private BlockPosition current_block_pos;

    // Start is called before the first frame update
    void Start()
    {
        block_behaviour = block.GetComponent<BlockBehaviour>();
        initMap(new char[,] {
            {'t', 't', 't', ' ', ' ', ' ', 't'},
            {'t', 't', 't', ' ', ' ', ' ', 't'},
            {' ', 't', 't', ' ', ' ', ' ', 't'},
            {'t', 't', 't', 't', 't', 't', 't'},
        });
        initBlock(0, 0);
    }

    public void initMap(char[,] map) {
        current_map = map;
        for (int y = 0; y < map.GetLength(0); y ++) {
            for (int x = 0; x < map.GetLength(1); x ++) {
                switch (map[y,x]) {
                    case ' ':
                    // air
                    break;
                    case 't':
                    addTile(x, y);
                    break;
                    default:
                    throw new System.Exception();
                }
            }
        }
    }

    private bool started = false;

    public void initBlock(int initx, int inity) {
        current_block_pos = new BlockPosition(initx, inity);
        current_block_pos.apply(block.transform);
        block.GetComponent<Rigidbody>().isKinematic = true;
        started = true;
    }

    void addTile(int x, int z) {
        GameObject new_tile = Instantiate(tile_pf, gameObject.transform);
        new_tile.transform.localPosition = new Vector3((float) x, 0f, (float) z);
    }

    private bool waiting_for_restart = false;
    private float restart_time = 0f;

    // Update is called once per frame
    void Update()
    {
        if (waiting_for_restart && Time.time >= restart_time) {
            waiting_for_restart = false;
            this.initBlock(0, 0);
            return;
        }
        if (!started || block_behaviour.isAnimating()) {
            return;
        }
        BlockPosition new_pos = null;
        if (Input.GetKeyDown("left")) {
            new_pos = new BlockPosition(current_block_pos);
            new_pos.xmax = current_block_pos.xmin;
            if (current_block_pos.standing()) {
                new_pos.xmin = new_pos.xmax - 2;
            } else if (current_block_pos.lying_x()) {
                new_pos.xmin = new_pos.xmax - 1;
            } else {
                new_pos.xmin = new_pos.xmax - 1;
            }
            block_behaviour.startRotationAnimation(new Vector3(current_block_pos.xmin, 0f, current_block_pos.ymin), new Vector3(0f,0f,1f), 90f, () => this.onBlockRotated());
        } else if (Input.GetKeyDown("right")) {
            new_pos = new BlockPosition(current_block_pos);
            new_pos.xmin = current_block_pos.xmax;
            if (current_block_pos.standing()) {
                new_pos.xmax = new_pos.xmin + 2;
            } else if (current_block_pos.lying_x()) {
                new_pos.xmax = new_pos.xmin + 1;
            } else {
                new_pos.xmax = new_pos.xmin + 1;
            }
            block_behaviour.startRotationAnimation(new Vector3(current_block_pos.xmax, 0f, current_block_pos.ymin), new Vector3(0f,0f,1f), -90f, () => this.onBlockRotated());
        } else if (Input.GetKeyDown("up")) {
            new_pos = new BlockPosition(current_block_pos);
            new_pos.ymin = current_block_pos.ymax;
            if (current_block_pos.standing()) {
                new_pos.ymax = new_pos.ymin + 2;
            } else if (current_block_pos.lying_x()) {
                new_pos.ymax = new_pos.ymin + 1;
            } else {
                new_pos.ymax = new_pos.ymin + 1;
            }
            block_behaviour.startRotationAnimation(new Vector3(current_block_pos.xmin, 0f, current_block_pos.ymax), new Vector3(1f,0f,0f), 90f, () => this.onBlockRotated());
        } else if (Input.GetKeyDown("down")) {
            new_pos = new BlockPosition(current_block_pos);
            new_pos.ymax = current_block_pos.ymin;
            if (current_block_pos.standing()) {
                new_pos.ymin = new_pos.ymax - 2;
            } else if (current_block_pos.lying_x()) {
                new_pos.ymin = new_pos.ymax - 1;
            } else {
                new_pos.ymin = new_pos.ymax - 1;
            }
            block_behaviour.startRotationAnimation(new Vector3(current_block_pos.xmin, 0f, current_block_pos.ymin), new Vector3(1f,0f,0f), -90f, () => this.onBlockRotated());
        }
        if (new_pos != null) {
            this.current_block_pos = new_pos;
        }
    }

    void onBlockRotated() {
        for (int x = current_block_pos.xmin; x < current_block_pos.xmax; x ++) {
            for (int y = current_block_pos.ymin; y < current_block_pos.ymax; y ++) {
                if (x < 0 || x >= current_map.GetLength(1) || y < 0 || y >= current_map.GetLength(0)) {
                    lose();
                    return;
                }
                switch (current_map[y,x]) {
                    case ' ':
                    lose();
                    break;
                }
            }
        }
    }

    void lose() {
        // initBlock(0, 0);
        Rigidbody rb = block.GetComponent<Rigidbody>();
        rb.isKinematic = false;
        started = false;
        waiting_for_restart = true;
        restart_time = Time.time + 2f;
    }
}
